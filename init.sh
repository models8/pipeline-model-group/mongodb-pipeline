for f in /mongo-scripts/*; do
  case "$f" in
    *.js) echo "$0: Running $f"; mongosh "$f"; echo ;;
    *)    echo "$0: Ignoring $f" ;;
  esac
  echo
done

